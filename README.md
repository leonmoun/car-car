# CarCar

---
CarCar is a application designed specifically for automobile dealerships to manage sales, service, employees (salespeople and technicians) and customers. It has been designed to be streamlined and efficient with minimal need for training.
CarCar is built on 3 separate microservice APIs and rendered onto a single page application.

* The Sales microservice manages customers, customer data, salespeople, and sales history.
* The Service microservice manages technicians, and creating/editing appointments.
* The Inventory microservice manages vehicles in stock.
---
Development Team:
* Nathan Axness - Automobile Service
* Leon Moun - Automobile Sales

---
## Requirements
* Docker
* Git
* Node.js 18.2 or above**
---
### Getting Started
1. Fork this repository
2. Clone the repository onto local machine using cmd: git clone "repository url here"
3. Start Docker desktop and run these commands to initialize:
4. Utilize http://localhost:3000 for the project frontend
---

### Design
![Design Diagram](ProjectBetaDiagram.png)

---
### Service microservice

The Service microservice manages technicians, and creating/editing appointments. A user is able to view a list of technicians, add and delete technicians, create and schedule service appointments as well as mark appointments "finished" or "canceled" (feature still in progress - please contact customer service for software update after 9/23/23) A user is also able to filter service histories by VIN (17-digit Vehicle Identification Number)

#### Appointments:
| Name          | Data Type         | Despription                           |
| --------------|:-----------------:|---------------------------------------|
| date_time     | CharField         | customer name input                   |
| reason        | CharField         | reason for vehicle service            |
| status        | Charfield         | appt canceled/finished status         |
| VIN           | CharField         | 17 digit Vehicle ID number            |
| technician    | ForeignKey Rel.   | technician assigned to service task   |

#### Appointment CRUD Endpoints:

| Action             | Method   | URL                                                       |
| ------------------ | ------   | ----------------------------------------------------------|
| Create Appointment | POST     | http://localhost:8080/api/appointments/                   |
| List Appointments  | GET      | http://localhost:8080/api/appointments/                   |
| Cancel Appointment | PUT      | http://localhost:8080/api/appointments/(apptID)/cancel/   |
| Finish Appointment | PUT      | http://localhost:8080/api/appointments/(apptID)/finish/   |
| Delete Appointment | DELETE   | http://localhost:8080/api/appointments/(apptID)/          |

#### Examples:
Create:
```
	{
		"date_time": "2021-01-06 15:30",
		"reason": "DEF delete",
		"vin": "122513331225",
		"customer": "Chuck Schumer",
		"status": "pending",
		"technician": "04"
	}

```
List:
```
{
	"appointments": [
		{
			"id": 17,
			"reason": "DEF delete",
			"vin": "122513331225",
			"customer": "Chuck Schumer",
			"status": "Pending",
			"technician": {
				"first_name": "Dave",
				"last_name": "Dilbeck",
				"employee_id": "04",
				"id": 4
			},
			"date_time": "2021-01-06T00:00:00+00:00"
		},
		{
			"id": 12,
			"reason": "DEF delete",
			"vin": "122513331225",
			"customer": "Chuck Schumer",
			"status": "finished",
			"technician": {
				"first_name": "Dave",
				"last_name": "Dilbeck",
				"employee_id": "04",
				"id": 4
			},
			"date_time": "2021-01-06T00:00:00+00:00"
		},
	]
}

```
Cancel return:
```
{
	"success": "appointment canceled"
}
```
Finish return:
```
{
	"success": "appointment completed"
}
```
Delete return:
```
{
	"deleted": true
}
```
#### Technicians:
| Name          | Data Type         | Despription                           |
| --------------|:-----------------:|---------------------------------------|
| first_name    | CharField         | tech first name                       |
| last_name     | CharField         | tech last name                        |
| employee_id   | Charfield         | tech employee id                      |

#### Technician CRUD Endpoints:

| Action             | Method   | URL                                                       |
| ------------------ | ------   | ----------------------------------------------------------|
| Create Technician  | POST     | http://localhost:8080/api/technicians/                    |
| List Technicians   | GET      | http://localhost:8080/api/technicians/                    |
| Delete Technician  | DELETE   | http://localhost:8080/api/technicians/(technicianID)/     |

#### Examples:
CREATE:
```
{
	"technicians": {
		"first_name": "Harry",
		"last_name": "High",
		"employee_id": "08",
		"id": 10
	}
}
```

LIST return:
```
{
	"technicians": [
		{
			"first_name": "Andersen",
			"last_name": "Axness",
			"employee_id": "01",
			"id": 1
		},
		{
			"first_name": "Bobby",
			"last_name": "Bouche",
			"employee_id": "02",
			"id": 2
		},
	]
}
```

---
# **Sales API**
---

The Sales API is a RESTful web service that allows users to manage automobile sales, salespeople, and customers.


## Endpoints

#### List Salespeople
```
Method: GET
URL: http://localhost:8090/api/salespeople/
Description: Retrieves a list of all salespeople.
```

#### Create a Salesperson
```
Method: POST
URL: http://localhost:8090/api/salespeople/
Description: Adds a new salesperson to the database.
```

#### Delete a Specific Salesperson
```
Method: DELETE
URL: http://localhost:8090/api/salespeople/:id
Description: Deletes an existing salesperson from the database.
```

#### List Customers
```
Method: GET
URL: http://localhost:8090/api/customers/
Description: Retrieves a list of all customers.
```

#### Create a Customer
```
Method: POST
URL: http://localhost:8090/api/customers/
Description: Adds a new customer to the database.
```

#### Delete a Specific Customer
```
Method: DELETE
URL: http://localhost:8090/api/customers/:id
Description: Deletes an existing customer from the database.
```

#### List Sales
```
Method: GET
URL: http://localhost:8090/api/sales/
Description: Retrieves a list of all automobile sales.
```

#### Create a Sale
```
Method: POST
URL: http://localhost:8090/api/sales/
Description: Adds a new sale to the database.
```

#### Delete a Sale
```
Method: DELETE
URL: http://localhost:8090/api/sales/:id
Description: Deletes an existing sale from the database.
```

#### Examples

Create a Salesperson (POST)

```json
{
	"first_name": "Joe",
	"last_name": "Biden",
	"employee_id": 1234
}
```
Example response:
```
{
  "id": 1,
  "first_name": "Joe",
  "last_name": "Biden",
  "employee_id": "1234"
}

```

Create a Customer (POST)

```json
{
  "first_name": "Barack",
  "last_name": "Obama",
  "address": "321 Sesame Street",
  "phone_number": "123-4567"
}
```
Example response:
```
{
  "id": 1,
  "first_name": "Barack",
  "last_name": "Obama",
  "address": "321 Sesame Street",
  "phone_number": "123-4567"
}
```

Create a Sale (POST)

```json
{

  "automobile": "ABC123",  // VIN of the automobile being sold
  "salesperson": 1,       // ID of the salesperson making the sale
  "customer": 1,          // ID of the customer buying the automobile
  "price": 25000.00
}

```
Example response:

```
{
  "id": 1,
  "automobile": {
    "vin": "123",
    "color": "Black",
    "year": "2023",
    "model": "BMW 340i",
    "sold": true
  },
  "salesperson": {
    "id": 1,
    "first_name": "Joe",
    "last_name": "Biden",
    "employee_id": "1234"
  },
  "customer": {
    "id": 1,
    "first_name": "Barack",
    "last_name": "Obama",
    "address": "321 Sesame Street",
    "phone_number": "123-4567"
  },
  "price": "69000.00"
}
```
---

# Models Overview
---
#### Salesperson Model
The Salesperson model represents a salesperson involved in the sales process. It contains the following fields:
```
id: The unique identifier for the salesperson.
first_name: The first name of the salesperson.
last_name: The last name of the salesperson.
employee_id: The employee ID of the salesperson.
```

#### Customer Model
The Customer model represents a customer who purchases an automobile. It contains the following fields:
```
id: The unique identifier for the customer.
first_name: The first name of the customer.
last_name: The last name of the customer.
address: The address of the customer.
phone_number: The phone number of the customer.
```
#### AutomobileVO Model

The "AutomobileVO" (Value Object) model is a representation of an automobile in the system. It serves as an intermediary between the backend database and the frontend user interface. The purpose of the "AutomobileVO" model is to encapsulate relevant data about an automobile in a structured format that can be easily consumed by the frontend application.

The key attributes of the "AutomobileVO" model typically include information such as the automobile's VIN (Vehicle Identification Number), color, year, model name, manufacturer name, and whether it is marked as sold or available.

#### AutomobileVO:
| Name          | Data Type         | Despription                           |
| --------------|:-----------------:|---------------------------------------|
| vin           | CharField         | 17 digit Vehicle ID                   |
| sold          | Boolean           | True/False sold status                |

```
vin: The Vehicle Identification Number (VIN) of the automobile (unique identifier).
color: The color of the automobile.
year: The manufacturing year of the automobile.
model: The foreign key reference to the related automobile model.
sold: A boolean field indicating whether the automobile is sold or not.
```
#### Sale Model

The Sale model represents a transaction where an automobile is sold to a customer by a salesperson. It contains the following fields:

```
id: The unique identifier for the sale.
automobile: The foreign key reference to the related automobile being sold.
salesperson: The foreign key reference to the salesperson making the sale.
customer: The foreign key reference to the customer who purchases the automobile.
price: The price of the sale.
```

---
# Inventory API
---
The Inventory API is a fully-accessible API that allows you to manage the automobile inventory for the automobile dealership. It provides RESTful endpoints for three main entities: Manufacturers, Vehicle Models, and Automobiles. 

## Endpoints
### Manufacturers
The Manufacturer model represents the company that manufactures the automobiles.
```
GET /api/manufacturers/: List all manufacturers.
POST /api/manufacturers/: Create a new manufacturer.
GET /api/manufacturers/:id/: Retrieve details of a specific manufacturer.
PUT /api/manufacturers/:id/: Update a specific manufacturer.
DELETE /api/manufacturers/:id/: Delete a specific manufacturer.
```
### Vehicle Models
The VehicleModel model represents the specific model of an automobile created by a Manufacturer.
```
GET /api/models/: List all vehicle models.
POST /api/models/: Create a new vehicle model.
GET /api/models/:id/: Retrieve details of a specific vehicle model.
PUT /api/models/:id/: Update a specific vehicle model.
DELETE /api/models/:id/: Delete a specific vehicle model.
```
### Automobiles
The Automobile model represents the actual automobile with a specific VehicleModel.
```
GET /api/automobiles/: List all automobiles.
POST /api/automobiles/: Create a new automobile.
GET /api/automobiles/:vin/: Retrieve details of a specific automobile.
PUT /api/automobiles/:vin/: Update a specific automobile.
DELETE /api/automobiles/:vin/: Delete a specific automobile.
```

### Prerequisites for Creating an Automobile
Before creating an automobile using the Automobile Inventory API, it is essential to ensure that the following prerequisites are met:

Manufacturer Existence: The automobile's manufacturer must be created and available in the API. Manufacturers represent the companies that manufacture the automobiles.

Vehicle Model Existence: The specific vehicle model of the automobile must be created and associated with the corresponding manufacturer. Vehicle models represent the different models of automobiles created by manufacturers.

Please ensure that you have created the necessary manufacturers and vehicle models before attempting to add a new automobile to the inventory. The API follows a relational data model, and the existence of manufacturers and vehicle models is a fundamental requirement for successfully creating and associating automobiles.

#### Create Manufacturer (Request Body):
```
{
  "name": "Chrysler"
}
```
 Example response:
```
{
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Chrysler"
}
```
#### Create Vehicle Model (Request Body):
```
{
  "name": "Sebring",
  "picture_url": "https://example.com/sebring.jpg",
  "manufacturer_id": 1
}
```
Example response:
```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "https://example.com/sebring.jpg",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}
```
#### Create Automobile (Request Body):
```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```
Example response:
```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "yellow",
  "year": 2013,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "https://example.com/sebring.jpg",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  },
  "sold": false
}
```
