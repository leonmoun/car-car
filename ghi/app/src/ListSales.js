import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

const ListSales = () => {
  const [sales, setSales] = useState([]);

  useEffect(() => {
    async function loadSales() {
      try {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
          const data = await response.json();
          setSales(data);
        } else {
          console.log(response);
        }
      } catch (error) {
        console.error('Error occurred while fetching sales:', error);
      }
    }

    loadSales();
  }, []);

  return (
  <div className="container mt-4">
  <div className="d-flex justify-content-between align-items-center mb-3">
    <h2>List of Sales</h2>
    <Link to="/add-sale" className="btn btn-primary">Add Sale</Link>
  </div>
      <table className="table table-bordered">
        <thead>
          <tr>
            <th>Salesperson Employee ID</th>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map((sale) => (
            <tr key={sale.id}>
              <td>{sale.salesperson.employee_id}</td>
              <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
              <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
              <td>{sale.automobile.vin}</td>
              <td>{sale.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ListSales;
