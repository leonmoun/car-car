import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

function AddAutomobileForm(props) {
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [models, setModels] = useState([]);
    const [selectedModel, setSelectedModel] = useState('');
    const navigate = useNavigate();

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleYearChange = (event) => {
        const value = event.target.value;
        setYear(value);
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleModelChange = (event) => {
        const value = event.target.value;
        setSelectedModel(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = selectedModel;

        const automobilesUrl = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(automobilesUrl, fetchConfig);

        if (response.ok) {
            const newAutomobile = await response.json();
            console.log(newAutomobile);
            setColor('');
            setYear('');
            setVin('');
            setSelectedModel('');
            navigate('/automobiles');
        } else{
            console.error('failed to add automobile', response.status, response.statusText);
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setModels(data.models);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className='container'>
        <div className='row'>
            <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                    <h1>Add an Automobile</h1>
                    <form onSubmit={handleSubmit} id="add-automobile-form">
                        <div className='form-floating mb-3'>
                            <input onChange={handleColorChange} value={color} placeholder='Color' required type='text' name='Color' id='color' className='form-control'></input>
                            <label htmlFor='color'>Color</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleYearChange} value={year} placeholder='Year' required type='text' name='Year' id='year' className='form-control'></input>
                            <label htmlFor='year'>Year</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleVinChange} value={vin} placeholder='Vin' required type='text' name='Vin' id='vin' className='form-control'></input>
                            <label htmlFor='vin'>VIN</label>
                        </div>
                        <div className='form-floating mb-3'>
                                <select onChange={handleModelChange} value={selectedModel} placeholder='model' required name='model' id='model' className='form-control'>
                                    <option value="">Select a Model</option>
                                        {models.map((model) => (
                                            <option key={model.id} value={model.id}>
                                                {model.name}
                                            </option>
                                        ))}
                                </select>
                            </div>
                        <button className='btn btn-primary'>Add</button>
                    </form>
                </div>
            </div>
        </div>
        </div>
    )
}

export default AddAutomobileForm;
