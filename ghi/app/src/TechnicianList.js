import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function TechnicianList(props) {
    const [technicians, setTechnician] = useState([])
    const navigate = useNavigate();
    async function loadTechnician() {
        const response = await fetch('http://localhost:8080/api/technicians/');
        if (response.ok) {
            const data = await response.json();
            setTechnician(data.technicians)
        } else {
            console.log(response);
        }
    }

    const handleDelete = async (technicianId) => {
        const response = await fetch(`http://localhost:8080/api/technicians/${technicianId}/`, {
            method: "DELETE",
        });
        if (response.ok) {
            loadTechnician()
        } else {
            console.error('failed to delete the technician', await response.text());
        }
    }

    useEffect(() => {
        loadTechnician();
    }, [])

    return (
        <div className="container mt-4">
            <div className="d-flex justify-content-between align-items-center mb-3">
                <h2>Technician List</h2>
                <button className="btn btn-primary" onClick={() => navigate('/technicians/create')}>
                    Add Technician
                </button>
            </div>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.map(technician => {
                        return (
                            <tr key={technician.employee_id}>
                                <td>{ technician.first_name }</td>
                                <td>{ technician.last_name }</td>
                                <td>{ technician.employee_id }</td>
                                <td>
                                    <button className='btn btn-danger' onClick={() => handleDelete(technician.id)}>Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default TechnicianList;
