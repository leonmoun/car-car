import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';


function AppointmentList(props) {
    const [appointments, setAppointments] = useState([])
    const [searchTerm, setSearchTerm] = useState('')
    const [filteredAppointments, setFilteredAppointments] = useState([]);
    const navigate = useNavigate();
    async function loadAppointments() {
    const response = await fetch('http://localhost:8080/api/appointments/');
    if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments)
        setFilteredAppointments(data.appointments);
    } else {
        console.log(response);
    }
}

    const handleCancel = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' }
        });
        const data = await response.json();
        if (response.ok) {
            setAppointments(appointments.map(appointment =>
              appointment.id === id ? { ...appointment, status: 'Canceled' } : appointment
              ));
        } else {
            console.error('appointment not updated to canceled', data);
        }
    }

    const handleFinish = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {
            method: "PUT",
            headers: { 'Content-Type': 'application/json' }
        });
        const data = await response.json();
        if (response.ok) {
            setAppointments(appointments.map(appointment =>
              appointment.id === id ? { ...appointment, status: 'Finished' } : appointment
              ));
        } else {
            console.error('appointment not updated to completed', data);
        }
    }

    const handleDelete = async (appointmentId) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${appointmentId}/`, {
            method: "DELETE",
        });
        if (response.ok) {
            loadAppointments()
        } else {
            console.error('failed to delete appointment from database', await response.text());
        }
    }

    useEffect(() => {
        loadAppointments();
    }, [])

    const handleSearchChange = event => {
      setSearchTerm(event.target.value);
    };

    const handleSearchSubmit = event => {
      event.preventDefault(); // prevent form from refreshing the page
      const filtered = appointments.filter(appointment =>
          String(appointment.vin).toLowerCase() === searchTerm.toLowerCase()
      );
      setFilteredAppointments(filtered);
  };

  //   const filteredAppointments = appointments.filter(appointment =>
  //     appointment.vin.toLowerCase().includes(searchTerm.toLowerCase())
  // );

    return (
      <div className="container mt-4">
          <div className="d-flex justify-content-between align-items-center mb-3">
            <h2>Current Appointments</h2>
            <form onSubmit={handleSearchSubmit}>
                <input type="text" className="form-control" placeholder="Search by VIN" value={searchTerm} onChange={handleSearchChange} />
                <button className="btn btn-primary" type="submit">Search</button>
                <button className="btn btn-primary" onClick={() => navigate('/appointments/create')}>Add Appointment</button>
            </form>
          </div>
          <table className='table table-striped'>
            <thead>
              <tr>
                <th>VIN</th>
                <th>Customer</th>
                <th>Date and Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {filteredAppointments.map(appointment => {
                return (
                  <tr key={appointment.id}>
                    <td>{ appointment.vin }</td>
                    <td>{ appointment.customer }</td>
                    <td>
                      {
                        (() => {
                          let date = new Date(appointment.date_time);
                          return `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()} ${date.getHours()}:${date.getMinutes() < 10 ? '0' : ''}${date.getMinutes()}`;
                        })()
                      }
                    </td>
                    <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                    <td>{ appointment.reason }</td>
                    <td>{ appointment.status }</td>
                    <td>
                      <button className="btn btn-warning" onClick={() => handleCancel(appointment.id)}>Cancel</button>
                    </td>
                    <td>
                      <button className='btn btn-success' onClick={() => handleFinish(appointment.id)}>Complete</button>
                    </td>
                    <td>
                      <button className='btn btn-danger' onClick={() => handleDelete(appointment.id)}>Delete</button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      );
}

export default AppointmentList;
