import React, { useState, useEffect } from 'react';

const SalespersonHistory = () => {
  const [salespeople, setSalespeople] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState(null);
  const [salesHistory, setSalesHistory] = useState([]);

  useEffect(() => {
    async function loadSalespeople() {
      try {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
          const data = await response.json();
          setSalespeople(data);
        } else {
          console.log(response);
        }
      } catch (error) {
        console.error('Error occurred while fetching salespeople:', error);
      }
    }

    loadSalespeople();
  }, []);

  const handleSalespersonChange = async (event) => {
    const selectedId = event.target.value;
    const selectedSalesperson = salespeople.find(salesperson => salesperson.id === parseInt(selectedId));
    setSelectedSalesperson(selectedSalesperson);
  
    try {
      const response = await fetch(`http://localhost:8090/api/sales/?salesperson_id=${selectedId}`);
      if (response.ok) {
        const data = await response.json();
        setSalesHistory(data);
      } else {
        console.log(response);
      }
    } catch (error) {
      console.error('Error occurred while fetching sales history:', error);
    }
  };
  return (
    <div className="container mt-4">
      <h2>Salesperson History</h2>
      <div className="form-group">
        <label htmlFor="salespersonSelect">Select Salesperson:</label>
        <select
          name="salesperson_id"
          className="form-control"
          id="salespersonSelect"
          onChange={handleSalespersonChange}
          value={selectedSalesperson ? selectedSalesperson.id : ''}
        >
          <option value="">--- Select Salesperson ---</option>
          {salespeople.map((salesperson) => (
            <option key={salesperson.id} value={salesperson.id}>
              {salesperson.first_name} {salesperson.last_name}
            </option>
          ))}
        </select>
      </div>

      {selectedSalesperson && (
        <>
          <h3>Sales History for {selectedSalesperson.first_name} {selectedSalesperson.last_name}</h3>
          <table className="table table-bordered">
            <thead>
              <tr>
                <th>Customer</th>
                <th>Automobile VIN</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              {salesHistory.map((sale) => (
                <tr key={sale.id}>
                  <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                  <td>{sale.automobile.vin}</td>
                  <td>{sale.price}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </>
      )}
    </div>
  );
};

export default SalespersonHistory;
