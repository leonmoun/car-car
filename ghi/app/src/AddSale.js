import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

const AddSale = () => {
  const [automobiles, setAutomobiles] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [selectedAutomobile, setSelectedAutomobile] = useState(null);
  const [selectedSalesperson, setSelectedSalesperson] = useState('');
  const [selectedCustomer, setSelectedCustomer] = useState('');
  const [price, setPrice] = useState('');

  const navigate = useNavigate();

  async function loadAutomobiles() {
    try {
      const response = await fetch('http://localhost:8100/api/automobiles/?sold=false');
      if (response.ok) {
        const data = await response.json();
        setAutomobiles(data.autos);
      } else {
        console.log(response);
      }
    } catch (error) {
      console.error('Error occurred while fetching automobiles:', error);
    }
  }

  async function loadSalespeople() {
    try {
      const response = await fetch('http://localhost:8090/api/salespeople/');
      if (response.ok) {
        const data = await response.json();
        setSalespeople(data);
      } else {
        console.log(response);
      }
    } catch (error) {
      console.error('Error occurred while fetching salespeople:', error);
    }
  }

  async function loadCustomers() {
    try {
      const response = await fetch('http://localhost:8090/api/customers/');
      if (response.ok) {
        const data = await response.json();
        setCustomers(data);
      } else {
        console.log(response);
      }
    } catch (error) {
      console.error('Error occurred while fetching customers:', error);
    }
  }

  useEffect(() => {
    loadAutomobiles();
    loadSalespeople();
    loadCustomers();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
  
    if (!selectedAutomobile || !selectedSalesperson || !selectedCustomer || !price) {
      alert('Please select all fields and enter the price.');
      return;
    }
  
    const automobileId = selectedAutomobile.vin; 
    const salespersonId = parseInt(selectedSalesperson);
    const customerId = parseInt(selectedCustomer); 
  
    const newSale = {
      automobile: automobileId,
      salesperson: salespersonId,
      customer: customerId,
      price: parseFloat(price),
    };
  
    try {
      const response = await fetch('http://localhost:8090/api/sales/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newSale),
      });
  
      if (response.ok) {
        const responseAutomobile = await fetch(`http://localhost:8100/api/automobiles/${automobileId}/`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ sold: true }),
        });
  
        if (!responseAutomobile.ok) {
          console.error('Failed to update automobile sold state.');
        }
  
        navigate('/list-sales');
        console.log(newSale);
      } else {
        console.error('Failed to add sale.');
      }
    } catch (error) {
      console.error('Error occurred while making the POST request:', error);
    }
  };

  return (
    <div className="container mt-4">
      <h2>Add Sale</h2>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label className="form-label">Select Automobile:</label>
          <select
            className="form-select"
            value={selectedAutomobile ? selectedAutomobile.vin : ''}
            onChange={(e) => {
              const selectedAuto = automobiles.find(auto => auto.vin === e.target.value);
              setSelectedAutomobile(selectedAuto);
            }}
            required>
            <option value="" disabled>
              Select an automobile
            </option>
            {automobiles.length > 0 &&
              automobiles.map((automobile) => (
                <option key={automobile.vin} value={automobile.vin}>
                  {`${automobile.year} ${automobile.model.manufacturer.name} ${automobile.model.name}`}
                </option>
              ))}
          </select>
        </div>
        <div className="mb-3">
          <label className="form-label">Select Salesperson:</label>
          <select
            className="form-select"
            value={selectedSalesperson}
            onChange={(e) => setSelectedSalesperson(e.target.value)}
            required
          >
            <option value="" disabled>Select a salesperson</option>
            {salespeople.map((salesperson) => (
              <option key={salesperson.id} value={salesperson.id}>
                {salesperson.first_name} {salesperson.last_name}
              </option>
            ))}
          </select>
        </div>
        <div className="mb-3">
          <label className="form-label">Select Customer:</label>
          <select
            className="form-select"
            value={selectedCustomer}
            onChange={(e) => setSelectedCustomer(e.target.value)}
            required
          >
            <option value="" disabled>Select a customer</option>
            {customers.map((customer) => (
              <option key={customer.id} value={customer.id}>
                {customer.first_name} {customer.last_name}
              </option>
            ))}
          </select>
        </div>
        <div className="mb-3">
          <label className="form-label">Sale Price:</label>
          <input
            type="number"
            className="form-control"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            required
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
  );
};

export default AddSale;
