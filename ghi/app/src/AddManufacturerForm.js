import React, { useEffect, useState } from 'react';

function AddManufacturerForm(props) {
    const [name, setName] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;

        const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(manufacturerUrl, fetchConfig);

        if (response.ok) {
            const newManufacturer = await response.json();
            console.log(newManufacturer);
            setName('');
        } else{
            console.error('failed to add manufacturer', response.status, response.statusText);
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className='container'>
        <div className='row'>
            <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                    <h1>Add a Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="add-manufacturer">
                        <div className='form-floating mb-3'>
                            <input onChange={handleNameChange} value={name} placeholder='Name' required type='text' name='Name' id='name' className='form-control'></input>
                            <label htmlFor='name'>Manufacturer Name</label>
                        </div>
                        <button className='btn btn-primary'>Add</button>
                    </form>
                </div>
            </div>
        </div>
        </div>
    )
}

export default AddManufacturerForm;
