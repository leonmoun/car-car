import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AddTechnicianForm from './AddTechnicianForm';
import AddSalesperson from './AddSalesperson';
import SalespeopleList from './SalespeopleList';
import CustomerList from './CustomerList';
import AddCustomer from './AddCustomer';
import AddSale from './AddSale';
import TechnicianList from './TechnicianList';
import AppointmentList from './AppointmentList';
import ListSales from './ListSales';
import SalespersonHistory from './SalespersonHistory';
import AddAppointmentForm from './AddAppointmentForm';
import ManufacturerList from './ManufacturerList';
import AddManufacturerForm from './AddManufacturerForm';
import ModelList from './ModelList';
import AddModel from './AddModel';
import AutomobilesList from './AutomobileList'
import AddAutomobileForm from './AddAutomobileForm'

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='/technicians/create' element={<AddTechnicianForm />} />
          <Route path='/technicians' element={<TechnicianList />} />
          <Route path='/appointments' element={<AppointmentList />} />
          <Route path='/appointments/create' element={<AddAppointmentForm />} />
          <Route path="/add-salesperson" element={<AddSalesperson />} />
          <Route path="/list-salespeople" element={<SalespeopleList />} />
          <Route path="/list-customers" element={<CustomerList />} />
          <Route path="/add-customer" element={<AddCustomer />} />
          <Route path="/manufacturers" element={<ManufacturerList />} />
          <Route path='/manufacturers/create' element={<AddManufacturerForm />} />
          <Route path="/models" element={<ModelList />} />
          <Route path="/models/create" element={<AddModel />} />
          <Route path="/add-sale" element={<AddSale />} />
          <Route path="/automobiles" element={<AutomobilesList />} />
          <Route path="/automobiles/create" element={<AddAutomobileForm />} />


          <Route path="/list-sales" element={<ListSales/>} />
          <Route path="/salesperson-history" element={<SalespersonHistory/>} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
