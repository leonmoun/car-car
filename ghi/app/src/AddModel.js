import React, { useEffect, useState } from 'react';

function AddModelForm(props) {
    const [modelName, setModel] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [manufacturers, setManufacturers] = useState([]);
    const [selectedManufacturer, setSelectedManufacturer] = useState('');

    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setSelectedManufacturer(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = modelName;
        data.picture_url = pictureUrl;
        data.manufacturer_id = selectedManufacturer;
        // manufacturer_id was from inventory views.py api_vehicle_models POST function it should either
        // match that or what is in the models
        const modelUrl = "http://localhost:8100/api/models/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(modelUrl, fetchConfig);

        if (response.ok) {
            const newModel = await response.json();
            console.log(newModel);
            setModel('');
            setPictureUrl('');
            setSelectedManufacturer('');
        } else{
            console.error('failed to add model', response.status, response.statusText);
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className='container'>
        <div className='row'>
            <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                    <h1>Add a Model</h1>
                    <form onSubmit={handleSubmit} id="add-model-form">
                        <div className='form-floating mb-3'>
                            <input onChange={handleModelChange} value={modelName} placeholder='Model' required type='text' name='Model' id='model' className='form-control'></input>
                            <label htmlFor='model'>Model Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handlePictureUrlChange} value={pictureUrl} placeholder='Picture URL' required type='text' name='PictureUrl' id='pictureUrl' className='form-control'></input>
                            <label htmlFor='pictureUrl'>Picture URL</label>
                        </div>
                        <div className='form-floating mb-3'>
                                <select onChange={handleManufacturerChange} value={selectedManufacturer} placeholder='manufacturer' required name='manufacturer' id='manufacturer' className='form-control'>
                                    <option value="">Select a Manufacturer</option>
                                        {/* {manufacturer.length > 0 && manufacturer.map((manufacturer) => ( */}
                                        {manufacturers.map((manufacturer) => (
                                            <option key={manufacturer.id} value={manufacturer.id}>
                                                {manufacturer.name}
                                            </option>
                                        ))}
                                </select>
                            </div>
                        <button className='btn btn-primary'>Add</button>
                    </form>
                </div>
            </div>
        </div>
        </div>
    )
}

export default AddModelForm;
