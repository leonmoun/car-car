import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

const AddSalesperson = () => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeId, setEmployeeId] = useState('');
  
  const navigate = useNavigate();

  const handleSubmit = async (event) => {
    event.preventDefault();
    
    const newSalesperson = {
      first_name: firstName,
      last_name: lastName,
      employee_id: employeeId
    };
    try {
      const response = await fetch('http://localhost:8090/api/salespeople/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newSalesperson)
      });

      if (response.ok) {
        navigate('/list-salespeople');
      } else {
        console.error('Failed to add salesperson.');
      }
    } catch (error) {
      console.error('Error occurred while making the POST request:', error);
    }
  };

  return (
    <div className="container mt-4">
      <h2>Add Salesperson</h2>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label className="form-label">First Name:</label>
          <input type="text" className="form-control" value={firstName} onChange={(e) => setFirstName(e.target.value)} />
        </div>
        <div className="mb-3">
          <label className="form-label">Last Name:</label>
          <input type="text" className="form-control" value={lastName} onChange={(e) => setLastName(e.target.value)} />
        </div>
        <div className="mb-3">
          <label className="form-label">Employee ID:</label>
          <input type="text" className="form-control" value={employeeId} onChange={(e) => setEmployeeId(e.target.value)} />
        </div>
        <button type="submit" className="btn btn-primary">Submit</button>
      </form>
    </div>
  );
}

export default AddSalesperson;
