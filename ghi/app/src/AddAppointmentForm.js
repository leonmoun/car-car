import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';


function AddAppointmentForm(props) {
    // const [date, setDate] = useState('');
    // const [time, setTime] = useState('');
    const [dateTime, setDateTime] = useState('');
    const [reason, setReason] = useState('');
    const [status, setStatus] = useState('');
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [technician, setTechnician] = useState("");
    const [technicians, setTechnicians] = useState([]);
    const navigate = useNavigate();

    // const handleDateChange = (event) => {
    //     const value = event.target.value;
    //     setDate(value);
    // }

    // const handleTimeChange = (event) => {
    //     const value = event.target.value;
    //     setTime(value);
    // }
    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        setDateTime(value);
    }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const handleStatusChange = (event) => {
        const value = event.target.value;
        setStatus(value);
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        // const dateTime = new Date(`${date}T${time}`).toISOString();

        const data = {
            date_time: dateTime,
            reason: reason,
            vin: vin,
            customer: customer,
            status: status,
            technician: technician,
        };
        console.log(data)
        try {
            const response = await fetch('http://localhost:8080/api/appointments/', {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(data)});
              navigate('/appointments');
        } catch (error){
            console.error(error)
        }

        // const response = await fetch(appointmentUrl, fetchConfig);

        // if (response.ok) {
        //     const newAppointment = await response.json();
        //     console.log('New appointment created:', newAppointment);
        //     setDate('');
        //     setTime('');
        //     setReason('');
        //     setStatus('');
        //     setVin('');
        //     setCustomer('');
        //     setTechnician("");
        // } else {
        //     console.error('failed to add technician', response.status, response.statusText);
        // }
    }

    const fetchData = async () => {
        try {
            const url = 'http://localhost:8080/api/technicians/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setTechnicians(data.technicians);
            } else {
                console.error("Failed to fetch technicians: response.status, response.statusText");
            }
        } catch (error) {
            console.error("Error fetching technicians:", error);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    console.log("Technicians array:", technicians);

    return (
        <>
        <div className='container'>
            <div className='row'>
                <div className='offset-3 col-6'>
                    <div className='shadow p-4 mt-4'>
                        <h1>Schedule an Appointment</h1>
                        <form onSubmit={handleSubmit} id="create-appointment-form">
                            {/* <div className='form-floating mb-3'>
                                <input onChange={handleDateChange} value={date} placeholder='Date' required type='date' name='Date' id='date' className='form-control'></input>
                                <label htmlFor='date'>Date</label>
                            </div>
                            <div className='form-floating mb-3'>
                                <input onChange={handleTimeChange} value={time} placeholder='Time' required type='time' name='Time' id='time' className='form-control'></input>
                                <label htmlFor='time'>Time</label>
                            </div> */}
                            <div>
                                <input onChange={handleDateTimeChange} value={dateTime} placeholder='Date and Time:' required type="datetime-local" name="dateTime" id="dateTime" />
                                <label htmlFor="dateTime">Date and Time:</label>
                            </div>
                            <div className='form-floating mb-3'>
                                <input onChange={handleReasonChange} value={reason} placeholder='Reason' required type='text' name='Reason' id='reason' className='form-control'></input>
                                <label htmlFor='reason'>Reason for service</label>
                            </div>
                            <div className='form-floating mb-3'>
                                <input onChange={handleStatusChange} value={status} placeholder='Status' required type='text' name='Status' id='status' className='form-control'></input>
                                <label htmlFor='status'>Status</label>
                            </div>
                            <div className='form-floating mb-3'>
                                <input onChange={handleVinChange} value={vin} placeholder='Vin' required type='text' name='Vin' id='vin' className='form-control'></input>
                                <label htmlFor='vin'>VIN</label>
                            </div>
                            <div className='form-floating mb-3'>
                                <input onChange={handleCustomerChange} value={customer} placeholder='Customer' required type='text' name='Customer' id='customer' className='form-control'></input>
                                <label htmlFor='customer'>Customer</label>
                            </div>
                            <div className='form-floating mb-3'>
                                <select onChange={handleTechnicianChange} value={technician} placeholder='technician' required name='technician' id='technician' className='form-control'>
                                    <option value="" disabled>Select a Technician</option>
                                        {technicians.length > 0 && technicians.map((technician) => (
                                            <option key={technician.id} value={technician.id}>
                                                {`${technician.first_name} ${technician.last_name}`}
                                            </option>
                                        ))}
                                </select>
                            </div>
                            <button className='btn btn-primary'>Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default AddAppointmentForm;
