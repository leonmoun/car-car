import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function ManufacturerList(props) {
    const [manufacturers, setManufacturers] = useState([])
    const navigate = useNavigate();
    async function loadManufacturers() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const data = await response.json();
            console.log('Data:', data);
            setManufacturers(data.manufacturers)
        } else {
            console.log(response);
        }
    }

    useEffect(() => {
        loadManufacturers();
    }, [])

    return (
        <div className="container mt-4">
            <div className="d-flex justify-content-between align-items-center mb-3">
                <h2>Manufacturer List</h2>
                <button className="btn btn-primary" onClick={() => navigate('/manufacturers/create')}>
                    Add Manufacturer
                </button>
            </div>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => {
                        return (
                            <tr key={manufacturer.id}>
                                <td>{ manufacturer.name }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default ManufacturerList;
