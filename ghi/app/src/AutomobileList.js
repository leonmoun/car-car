import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function AutomobileList() {
    const [automobiles, setAutomobiles] = useState([]);
    const navigate = useNavigate();
  
    async function loadAutomobiles() {
      try {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
          const data = await response.json();
          console.log('Data received:', data);
          setAutomobiles(data.autos)
        } else {
          console.log(response);
        }
      } catch (error) {
        console.error('Error occurred while fetching automobiles:', error);
      }
    }

  useEffect(() => {
    loadAutomobiles();
  }, []);

  return (
    <div className="container mt-4">
      <div className="d-flex justify-content-between align-items-center mb-3">
        <h2>Automobile List</h2>
        <button className="btn btn-primary" onClick={() => navigate('/automobiles/create')}>
          Add Automobile
        </button>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map((automobile) => (
            <tr key={automobile.id}>
              <td>{automobile.vin}</td>
              <td>{automobile.color}</td>
              <td>{automobile.year}</td>
              <td>{automobile.model.name}</td>
              <td>{automobile.model.manufacturer.name}</td>
              <td>{automobile.sold ? 'Yes' : 'No'}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default AutomobileList;
