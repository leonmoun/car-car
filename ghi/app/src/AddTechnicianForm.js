import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

function AddTechnicianForm(props) {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [employee_id, setEmployeeId] = useState('');
    const navigate = useNavigate();

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleEmployeeIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;

        const technicianUrl = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(technicianUrl, fetchConfig);

        if (response.ok) {
            const newTechnician = await response.json();
            console.log(newTechnician);
            setFirstName('');
            setLastName('');
            setEmployeeId('');
            navigate('/technicians');
        } else{
            console.error('failed to add technician', response.status, response.statusText);
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className='container'>
        <div className='row'>
            <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                    <h1>Add a Technician</h1>
                    <form onSubmit={handleSubmit} id="add-technician-form">
                        <div className='form-floating mb-3'>
                            <input onChange={handleFirstNameChange} value={first_name} placeholder='First Name' required type='text' name='First Name' id='first_name' className='form-control'></input>
                            <label htmlFor='first_name'>Technician's First Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleLastNameChange} value={last_name} placeholder='Last Name' required type='text' name='Last Name' id='last_name' className='form-control' />
                            <label htmlFor='last_name'>Technician's Last Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleEmployeeIdChange} value={employee_id} placeholder='Employee ID' required type='text' name='Employee ID' id='employee_id' className='form-control' />
                            <label htmlFor='employee_id'>Technician's Employee ID</label>
                        </div>
                        <button className='btn btn-primary'>Add</button>
                    </form>
                </div>
            </div>
        </div>
        </div>
    )
}

export default AddTechnicianForm;
