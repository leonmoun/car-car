import React from 'react';
import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className='nav-item'>
              <NavLink className="nav-link" aria-current="page" to='/'>Home</NavLink>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Service
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><NavLink className="dropdown-item" activeclassname="active" to="/appointments/create">Schedule an Appointment</NavLink></li>
                <li><NavLink className="dropdown-item" activeclassname="active" to="/appointments">View Appointments</NavLink></li>
                <li><NavLink className="dropdown-item" activeclassname="active" to="/technicians">Technicians</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><NavLink className="dropdown-item" activeclassname="active" to="/list-salespeople">Our Team</NavLink></li>
                <li><NavLink className="dropdown-item" activeclassname="active" to="/list-customers">Our Customers</NavLink></li>
                <li><NavLink className="dropdown-item" activeclassname="active" to="/list-sales">Sales List</NavLink></li>
                <li><NavLink className="dropdown-item" activeclassname="active" to="/salesperson-history">Salesperson History</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Inventory
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><NavLink className="dropdown-item" activeclassname="active" to="/manufacturers">Manufacturers</NavLink></li>
                <li><NavLink className="dropdown-item" activeclassname="active" to="/models">Models</NavLink></li>
                <li><NavLink className="dropdown-item" activeclassname="active" to="/automobiles">Automobiles</NavLink></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
