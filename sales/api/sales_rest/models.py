from django.db import models

class Salesperson(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=20)

    def __str__(self):
        return f"{self.first_name} {self.last_name} (ID: {self.employee_id})"

class Customer(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=15)

    def __str__(self):
        return f"{self.first_name} {self.last_name} (Phone: {self.phone_number})"

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, primary_key=True) 
    sold = models.BooleanField(default=False)

    def __str__(self):
        return f"VIN: {self.vin}, Sold: {self.sold}"

class Sale(models.Model):
    automobile = models.ForeignKey(AutomobileVO, on_delete=models.CASCADE)
    salesperson = models.ForeignKey(Salesperson, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return f"Sale of {self.automobile} by {self.salesperson} to {self.customer} for ${self.price}"
