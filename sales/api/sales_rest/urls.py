from django.urls import path
from . import views

urlpatterns = [
    path('salespeople/', views.salesperson_list_create_view, name='salesperson-list-create'),
    path('salespeople/<int:pk>/', views.salesperson_delete_view, name='salesperson-delete'),
    path('customers/', views.customer_list_create_view, name='customer-list-create'),
    path('customers/<int:pk>/', views.customer_delete_view, name='customer-delete'),
    path('sales/', views.sale_list_create_view, name='sale-list-create'),
    path('sales/<int:pk>/', views.sale_delete_view, name='sale-delete'),
]
